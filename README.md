## REST API for Customer Entity
this is a prototype webservice with CRUD operations.

---
***setup project***
```
docker-compose up -d && ./bin/composer.sh install
```

***The API will be avalaible at:***
```
http://localhost:8888
```

**Routes**  

| **Action** | Method | **Endpoint**    |
| :--------- | :----- | :-------------- |
| LIST       | GET    | /customers      |
| READ       | GET    | /customers/{id} |
| CREATE     | POST   | /customers      |
| UPDATE     | PUT    | /customers/{id} |
| DELETE     | DELETE | /customers/{id} |

Example JSON data can be used for POST and PUT: 
```
{
  "name": "example customer name"
}
```
