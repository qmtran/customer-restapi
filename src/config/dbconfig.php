<?php

return [
    'DB_HOST' => 'database',
    'DB_NAME' => 'customer_db',
    'DB_USER' => 'dbuser',
    'DB_PASSWORD' => 'dbpassword',
    'DB_PORT' => '3306'
];