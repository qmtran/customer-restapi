<?php

namespace App\Module\Customer\Model;

use App\Core\DB\MysqlConnect;
use PDO;

class Customer
{
    private $db;
    private $table = 'customer';

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $dbConnection = new MysqlConnect();
        $this->db = $dbConnection->getDbConnection();
    }

    /**
     * @param $data
     * @return bool
     */
    public function createCustomer($data)
    {
        if ($this->checkDataExist($data)) {
            return false;
        }

        $sql = "INSERT INTO $this->table (name) VALUES ('".$data['name']."')";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @param bool $id
     * @return array
     */
    public function getCustomer($id = false)
    {
        $sql = "SELECT * FROM $this->table";
        ($id && !empty($id)) ? $sql .= " WHERE id = $id" : "";

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function editCustomer($id, $data)
    {
        $sql = "UPDATE $this->table SET name='".$data['name']."' WHERE id='".$id."' ";
        $stmt = $this->db->prepare($sql);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteCustomer($id)
    {
        $sql = "DELETE FROM $this->table WHERE id='".$id."' ";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    protected function checkDataExist($data)
    {
        $sql = "SELECT * FROM $this->table WHERE name='".$data['name']."'";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}