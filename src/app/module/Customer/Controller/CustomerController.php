<?php

namespace App\Module\Customer\Controller;

use App\Core\AbstractController;
use App\Module\Customer\Model\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function list()
    {
        $customer = new Customer();
        $data = $customer->getCustomer();

        return $this->outputJson($data);
    }

    public function create(Request $request)
    {
        $customer = new Customer();
        $response = new Response();

        if ($customer->createCustomer(json_decode($request->getContent(), true))) {
            $response->setStatusCode(Response::HTTP_CREATED);
            $response->setContent('Success, customer created!');
        } else {
            $response->setStatusCode(Response::HTTP_PRECONDITION_FAILED);
            $response->setContent('Failed, cannot create new customer!');
        }
        return $response;
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show($id)
    {
        $customer = new Customer();
        $data = $customer->getCustomer($id);

        return $this->outputJson($data);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $customer = new Customer();
        $response = new Response();

        if ($customer->editCustomer(
                $request->attributes->get('id'),
                json_decode($request->getContent(), true)
            )) {

            $response->setStatusCode(Response::HTTP_CREATED);
            $response->setContent('Success, changes saved!');
        } else {
            $response->setStatusCode(Response::HTTP_PRECONDITION_FAILED);
            $response->setContent('Failed, cannot edit new customer!');
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request)
    {
        $customer = new Customer();
        $response = new Response();

        if ($customer->deleteCustomer($request->attributes->get('id'))) {
            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent('Success, customer deleted!');
        } else {
            $response->setStatusCode(Response::HTTP_PRECONDITION_FAILED);
            $response->setContent('Failed, no data found!');
        }
        return $response;
    }
}