<?php

namespace App\Module\App\Controller;

use App\Core\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        return $this->outputJson([
            'service' => 'ok'
        ]);
    }
}