<?php

namespace App\Core;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController
{
    protected function outputJson($data)
    {
        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }
}