<?php

namespace App\Core\DB;

use PDO;

class MysqlConnect
{
    public $db;

    public function getDbConnection()
    {
        $dsn = 'mysql:host=' . $GLOBALS['config']['DB_HOST'] .
            ';dbname=' . $GLOBALS['config']['DB_NAME'] .
            ';port='   . $GLOBALS['config']['DB_PORT'] .
            ';connect_timeout=15';

        $user = $GLOBALS['config']['DB_USER'];
        $password = $GLOBALS['config']['DB_PASSWORD'];

        try {
            $this->db = new PDO($dsn, $user, $password);
            $this->db->exec("SET NAMES utf8");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $this->db;
    }
}