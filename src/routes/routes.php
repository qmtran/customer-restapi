<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use App\Customer\Controller\CustomerController;

$routes = new RouteCollection();
$routes->add('index', new Route('/', [
    '_controller' => 'App\Module\App\Controller\IndexController::index'
]));

$customerRoutes = new RouteCollection();
$customerRoutes->addPrefix('/api');
$customerRoutes->add('customers_list', new Route('/customers', [
    '_controller' => 'App\Module\Customer\Controller\CustomerController::list'
], [], [], '', [], ['GET']));

$customerRoutes->add('customers_create', new Route('/customers', [
    '_controller' => 'App\Module\Customer\Controller\CustomerController::create'
], [], [], '', [], ['POST']));

$customerRoutes->add('customers_show', new Route('/customers/{id}', [
    '_controller' => 'App\Module\Customer\Controller\CustomerController::show'
], [], [], '', [], ['GET']));

$customerRoutes->add('customers_update', new Route('/customers/{id}', [
    '_controller' => 'App\Module\Customer\Controller\CustomerController::update'
], [], [], '', [], ['PUT']));

$customerRoutes->add('customers_delete', new Route('/customers/{id}', [
    '_controller' => 'App\Module\Customer\Controller\CustomerController::delete'
], [], [], '', [], ['DELETE']));

$routes->addCollection($customerRoutes);

return $routes;